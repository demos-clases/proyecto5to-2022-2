﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DataLayer.Servicios.Productos
{
    public class MarcaService: ICrudService<TblMarca>
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly ReservasDbContext _db;

        public MarcaService(
            IConfiguration configuration,
            ILogger logger,
            ReservasDbContext db)
        {
            _db = db;
        }


        public async Task createAsync(TblMarca body)
        {
            await _db.AddAsync(body);
            await _db.SaveChangesAsync();
        }

    
        public Task<bool> deleteAsync(int id)
        {
            throw new NotImplementedException();
        }



        public async Task<TblMarca> getAsync(int id)
        {
            TblMarca marca = await _db.Marcas.FindAsync(id);
            return marca;
        }

 

        public async Task<List<TblMarca>> listAllAsync()
        {
            return await _db.Marcas.ToListAsync();
        }


        public Task<bool> updateAsync(int id, TblMarca body)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateImageAsync( int id, string path)
        {
            TblMarca marca = await getAsync(id);
            marca.logoUrl = path;
            int res = await _db.SaveChangesAsync();
            if (res <= 0) throw new Exception("No se pudo actualizar la imagen");
        }
    }
}
