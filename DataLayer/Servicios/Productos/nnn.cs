﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Servicios.Productos
{
    public class nnn
    {
        private readonly ReservasDbContext _db;
        public nnn(ReservasDbContext db)
        {
            _db = db;
        }

        public string getData()
        {
            var marcas = _db.Marcas.ToList();
            var prods = _db.Productos.ToList();
            return "";
        }
        public async Task<string> getDataAsync()
        {
            List<TblMarca> marcas = await _db.Marcas.ToListAsync();
            List<TblProducto> prods = await _db.Productos.ToListAsync();
            //marcas.Join(prods, new { }, () => { });
            return "";
        }

        public async Task<string> getDataComplexAsync( )
        {
            List<TblMarca> marcas = await _db.Marcas.ToListAsync();
            List<TblProducto> prods = await _db.Productos.Where(
                p => p.marcaId == marcas[0].id
                ).ToListAsync();
            //marcas.Join(prods, new { }, () => { });
            return "";
        }
    }
}
