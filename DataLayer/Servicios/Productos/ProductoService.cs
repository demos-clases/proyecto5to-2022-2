﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DataLayer.Servicios.Productos
{
    public class ProductoService: ICrudService<Producto>
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly ReservasDbContext _db;

        public ProductoService(
            IConfiguration configuration,
            ILogger logger,
            ReservasDbContext db)
        {
            _db = db;
            _logger = logger;
            _configuration = configuration;
        }

        #region productos
        public async Task<List<Producto>> ListProductosByMarcaAsync(
            int marcaId)
        {
            _logger.LogInformation($"Buscando los " +
                $"productos de la marca {marcaId}");

            return await _db.Productos
                .Where((prod) =>
                    prod.marcaId == marcaId
                    //prod.nombre.Contains("choco")
                    //&& prod.precioVenta > 10
                )
                .Select( (object prod) => (Producto)prod)
                .ToListAsync();
        }

        public async Task<List<Producto>> listAllAsync()
        {
            var res = await _db.Productos
                .GroupJoin(
                    _db.ImagenesProducto,
                    (prod) => prod.id,
                    (img) => img.productoId,
                    (prod, img) => new {prod, img})
                .SelectMany(
                    temp => temp.img.DefaultIfEmpty(),
                    (data, temp) => new
                    {
                        id = data.prod.id,
                        marcaId = data.prod.marcaId,
                        nombre = data.prod.nombre,
                        precioVenta = data.prod.precioVenta,
                        sku = data.prod.sku,
                        imagenUrl = temp.imagenUrl
                    })
                .ToListAsync();

            List<Producto> productos = new List<Producto>();
            // transformas la respuesta de BD
            // al formato de trabajo final
            List<int> ids = res
                .Select((r) => r.id)
                .Distinct()
                .ToList();
            foreach (int id in ids)
            {
                var prod = res.FirstOrDefault(
                    (p) => p.id == id);
                List<string> imgs = new List<string>();

                imgs = res.Where(p => p.id == id)
                   .Select((p) => p.imagenUrl)
                   .ToList();

                Producto p = new Producto
                {
                    id = prod.id,
                    marcaId = prod.marcaId,
                    nombre = prod.nombre,
                    precio = prod.precioVenta,
                    sku = prod.sku,
                    imagenes = imgs
                };
                productos.Add(p);
            }


            return productos;
        }

        public async Task<Producto> getAsync(int id)
        {
            TblProducto producto = await _db.Productos.FindAsync(id);
            return (Producto)producto;
        }

        public async Task<bool> deleteAsync(int id)
        {
            TblProducto producto = await _db.Productos.FindAsync(id);
            var res = _db.Productos.Remove(producto);
            return true;
        }
        
        public Task<bool> updateAsync(int id, Producto body)
        {
            throw new NotImplementedException();
        }

        public async Task createAsync(Producto body)
        {
            TblProducto producto = new TblProducto {
                marcaId = body.marcaId,
                nombre = body.nombre,
                precioVenta = body.precio,
                sku = body.sku
            };
            await _db.AddAsync(producto);
            int res = await _db.SaveChangesAsync();
            int a = res;
        }

        #endregion productos

        #region imagenes
        public async Task AddImageToProduct(int productId, string pathImagen)
        {
            ImagenProductoService service = new ImagenProductoService(
                _configuration, _logger, _db);
            TblImagenProducto img = new TblImagenProducto {
                imagenUrl = pathImagen,
                productoId = productId,
            };
            await service.createAsync(img);
        }

        #endregion
    }
}
