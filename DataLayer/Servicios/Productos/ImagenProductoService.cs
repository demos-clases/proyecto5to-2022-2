﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TblImagenProducto = DataLayer.Entities.ReservasDb.Tables.TblImagenProducto;

namespace DataLayer.Servicios.Productos
{
    public class ImagenProductoService: ICrudService<TblImagenProducto>
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly ReservasDbContext _db;

        public ImagenProductoService(
            IConfiguration configuration,
            ILogger logger,
            ReservasDbContext db)
        {
            _db = db;
            _logger = logger;
            _configuration = configuration;
        }

        public async Task createAsync(TblImagenProducto body)
        {
            await _db.AddAsync(body);
            int res = await _db.SaveChangesAsync();
            if (res < 0) throw new Exception("No se subio la imagen");
        }

        public Task<bool> deleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<TblImagenProducto> getAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<TblImagenProducto>> listAllAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<List<TblImagenProducto>> ListImagesByProductAsync(
            int productId)
        {
            _logger.LogInformation($"Buscando imagenes por " +
                $"productos  {productId}");

            return await _db.ImagenesProducto
                .Where((img) =>
                    img.productoId == productId
                ).ToListAsync();
        }

        public Task<bool> updateAsync(int id, TblImagenProducto body)
        {
            throw new NotImplementedException();
        }
    }
}
