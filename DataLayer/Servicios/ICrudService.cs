﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataLayer.Servicios
{
    public interface ICrudService<T>
    {
        public Task<List<T>> listAllAsync();
        public Task<T> getAsync(int id);
        public Task<bool> updateAsync(int id, T body);
        public Task<bool> deleteAsync(int id);
        public Task createAsync(T body);
    }
}
