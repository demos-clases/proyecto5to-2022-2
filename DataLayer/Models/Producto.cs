﻿using System;
using System.Collections.Generic;
using DataLayer.Entities.ReservasDb.Tables;

namespace DataLayer.Models
{
	public class Producto
	{
		public int id { get; set; }
		public int marcaId { get; set; }
		public string nombre { get; set; }
		public decimal precio { get; set; }
		public string sku { get; set; }
		public List<string> imagenes { get; set; }


		public static implicit operator Producto(TblProducto table)
        {
			if (table == null) return null;
			return new Producto
			{
				id = table.id,
				marcaId = table.marcaId,
				nombre = table.nombre,
				precio = table.precioVenta,
				sku = table.sku,
			};
        }
	}
}

