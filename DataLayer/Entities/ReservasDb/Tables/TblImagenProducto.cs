﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entities.ReservasDb.Tables
{
	[Table("ImagenProducto")]
	public class TblImagenProducto
	{
		[Key]
		public int id { get; set; }
		public int productoId { get; set; }
		public string imagenUrl { get; set; }
	}
}

