﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entities.ReservasDb.Tables
{
    [Table("Puesto")]
    public class TblPuesto
    {
        [Key]
        public int id { get; set; }
        public string nombre { get; set; }
        public bool esAdministrador { get; set; }
    }
}
