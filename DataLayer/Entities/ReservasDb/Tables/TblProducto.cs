﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entities.ReservasDb.Tables
{
    [Table("Producto")]
    public class TblProducto
    {
        [Key]
        public int id { get; set; }
        public int marcaId { get; set; }
        public string nombre { get; set; }
        public decimal precioVenta { get; set; }
        public string sku { get; set; }
        // public ICollection<TblImagenProducto> imagenes { get; set; }
    }
}
