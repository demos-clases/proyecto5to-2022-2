﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entities.ReservasDb.Tables
{
    [Table("Marca")]
    public class TblMarca
    {
        [Key]
        public int id { get; set; }
        public string nombre { get; set; }
        public string logoUrl { get; set; }

    }
}
