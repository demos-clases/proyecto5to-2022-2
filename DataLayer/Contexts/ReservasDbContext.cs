﻿using System;
using DataLayer.Entities.ReservasDb.Tables;
using Microsoft.EntityFrameworkCore;
//using System.Data.Entity;

namespace DataLayer.Contexts
{
    public class ReservasDbContext: DbContext
    {
        public ReservasDbContext(
            DbContextOptions<ReservasDbContext> options) : base(options)
        {
        }

        public DbSet<TblPuesto> Puestos { get; set; }
        public DbSet<TblProducto> Productos { get; set; }
        public DbSet<TblMarca> Marcas { get; set; }
        public DbSet<TblImagenProducto> ImagenesProducto { get; set; }
    }
}
