﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Proyecto5to.Controllers.work
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class puestoController: ControllerBase
    {
        private readonly ReservasDbContext _db;
        public puestoController(ReservasDbContext context)
        {
            _db = context;
        }

        [HttpGet]
        [Produces("application/json")]
        [Route("puestos")]
        public async Task<ActionResult<List<TblPuesto>>> GetPuestosAsync()
        {
            List<TblPuesto> puestos = await _db.Puestos.ToListAsync();
            return Ok(puestos);
        }

    }
}
