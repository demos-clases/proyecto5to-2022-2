﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using DataLayer.Servicios.Productos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Proyecto5to.Controllers.v1.Productos
{
    [Route("/api/v1/marca")]
    public class MarcaController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ProductoController> _logger;
        private readonly ReservasDbContext _context;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly MarcaService _marcaService;

        public MarcaController(
            IConfiguration configuration,
            ILogger<ProductoController> logger,
            IWebHostEnvironment appEnvironment,
            ReservasDbContext context
            )
        {

            _configuration = configuration;
            _logger = logger;
            _context = context;
            _appEnvironment = appEnvironment;
            _marcaService = new MarcaService(
                _configuration, _logger, _context);
        }

        [HttpGet]
        [Produces("application/json")]
        [Route("")]
        public async Task<ActionResult<List<TblMarca>>> GetMarcasAsync()
        {

            return Ok(await _marcaService.listAllAsync());
        }

        [HttpGet]
        [Produces("application/json")]
        [Route("{id}")]
        public async Task<ActionResult<TblMarca>> GetMarcaAsync(
            [FromRoute] int id)
        {
            TblMarca marca = await _marcaService.getAsync(id);
            return marca == null
                ? NotFound()
                : Ok(marca);
        }

        [HttpPost]
        [Produces("application/json")]
        [Route("")]
        public async Task<ActionResult> PostProductosByMarcasAsync(
            [FromBody] TblMarca model
            )
        {
            await _marcaService.createAsync(model);

            return NoContent();
        }

        #region imagenes
        [HttpPost]
        [Produces("application/json")]
        [Route("{id}/imagen")]
        public async Task<ActionResult> AddImageMarcaAsync(
            [FromRoute] int id,
            [FromForm] ImageRequest model)
        {
            string path = await CopyImageAsync(model.image, id);
            await _marcaService.UpdateImageAsync(id, path);
            return NoContent();
        }

        private async Task<string> CopyImageAsync(IFormFile image, int id)
        {

            List<string> aceptedMimeTypes = new List<string> { "image/jpg", "image/jpeg", "image/png" };
            string mimetype = image.ContentType;
            if (aceptedMimeTypes.Where(m => m == mimetype).Count() <= 0)
            {
                throw new Exception("Tipo de archivo invalido");
            }
            long size = image.Length;

            if (size > 0)
            {
                string ext = image.FileName.Split('.').LastOrDefault();
                string wwwroot = _appEnvironment.WebRootPath;
                string nameImage = $"marca-{id}.{ext}";
                string folder = Path.Combine(wwwroot, "uploads", "brands", nameImage);
                using (var stream = System.IO.File.Create(folder))
                {
                    await image.CopyToAsync(stream);
                }
                return $"/uploads/brands/{nameImage}";
            }
            throw new Exception("no es un archivo");
        }

        #endregion imagenes

    }

    
}

