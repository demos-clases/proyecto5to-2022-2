﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Contexts;
using DataLayer.Entities.ReservasDb.Tables;
using DataLayer.Models;
using DataLayer.Servicios.Productos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Proyecto5to.Controllers.v1.Productos
{
    [Route("/api/v1/productos")]
    public class ProductoController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ProductoController> _logger;
        private readonly ReservasDbContext _context;
        private readonly ProductoService _productoService;
        private readonly IWebHostEnvironment _appEnvironment;


        public ProductoController(
            IConfiguration configuration,
            ILogger<ProductoController> logger,
            IWebHostEnvironment appEnvironment,
            ReservasDbContext context
            )
        {
            
            _configuration = configuration;
            _logger = logger;
            _context = context;
            _appEnvironment = appEnvironment;
            _productoService = new ProductoService(
                _configuration, _logger, _context);
        }

        #region producto

        [HttpGet]
        [Produces("application/json")]
        [Route("")]
        public async Task<ActionResult<List<Producto>>> GetProductosAsync()
        {

            return Ok(await _productoService.listAllAsync());
        }

        [HttpPost]
        [Produces("application/json")]
        [Route("")]
        public async Task<ActionResult> PostProductosByMarcasAsync(
            [FromBody] Producto model
            )
        {
            await _productoService.createAsync(model);

            return NoContent();
        }

  

        [HttpGet]
        [Produces("application/json")]
        [Route("marcas/{marcaId}/productos")]
        public async Task<ActionResult<List<TblProducto>>> GetProductosByMarcasAsync(
            [FromRoute] int marcaId
            )
        {
            ProductoService service = new ProductoService(
                _configuration, _logger, _context);

            return Ok(await service.ListProductosByMarcaAsync(marcaId));
        }
        #endregion producto

        #region imagenes
        [HttpPost]
        [Produces("application/json")]
        [Route("{id}/imagen")]
        public async Task<ActionResult> AddImageProductosAsync(
            [FromRoute] int id,
            [FromForm] ImageRequest model)
        {
            string path = await CopyImageAsync(model.image, id);
            await _productoService.AddImageToProduct(id, path);
            return NoContent();
        }

        private async Task<string> CopyImageAsync(IFormFile image, int id)
        {
            List<string> aceptedMimeTypes = new List<string> { "image/jpg", "image/jpeg", "image/png" };
            string mimetype = image.ContentType;
            if (aceptedMimeTypes.Where(m => m == mimetype).Count() <= 0)
            {
                throw new Exception("Tipo de archivo invalido");
            }
            long size = image.Length;

            if (size > 0)
            {
                string ext = image.FileName.Split('.').LastOrDefault();
                string wwwroot = _appEnvironment.WebRootPath;
                var uuid = Guid.NewGuid();
                string nameImage = $"producto-{id}-{uuid.ToString()}.{ext}";
                string folder = Path.Combine(wwwroot, "uploads", "products", nameImage);
                using (var stream = System.IO.File.Create(folder))
                {
                    await image.CopyToAsync(stream);
                }
                return $"/uploads/products/{nameImage}";
            }
            throw new Exception("no es un archivo");
        }

        #endregion imagenes
    }

    public class ImageRequest
    {
        public IFormFile image { get; set; }
    }
}
